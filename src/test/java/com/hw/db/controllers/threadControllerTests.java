package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.controllers.threadController;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class threadControllerTests {
    private Thread thread;

    @BeforeEach
    @DisplayName("thread creation test")
    void createThreadTest() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        thread = new Thread("somebody", timestamp, "forum", "message", "slug", "title", 5);
        thread.setId(0);
    }

    @Test
    @DisplayName("Test on checking id or slug")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController tc = new threadController();

            assertEquals(thread, tc.CheckIdOrSlug("slug"));
            assertEquals(thread, tc.CheckIdOrSlug("0"));
        }
    }

    @Test
    @DisplayName("Test create post")
    void testCreatePost() {
        List<Post> posts = new ArrayList<>();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController tc = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), tc.createPost("slug", posts));
        }
    }

    @Test
    @DisplayName("Test get posts")
    void testGetPosts() {
        List<Post> posts = new ArrayList<>();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController tc = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), tc.Posts("slug", 10, 1, "sort", true));
        }
    }

    @Test
    @DisplayName("Test change")
    void testChange() {
        Message notFoundMessage = new Message("Раздел не найден.");
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Thread changedThread = new Thread("someone", timestamp, "newForum", "message", "newSlug", "title", 6);
        changedThread.setId(1);
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            String slugId = thread.getId().toString();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slugId)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("mistake")).thenReturn(null);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(changedThread);
            threadMock.when(() -> ThreadDAO.change(null, changedThread)).thenThrow(new DataAccessException("Раздел не найден") {});

            threadController tc = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(notFoundMessage).getStatusCode(), tc.change("mistake", changedThread).getStatusCode());
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(changedThread).getStatusCode(), tc.change("slug", changedThread).getStatusCode());
        }
    }

    @Test
    @DisplayName("Test info")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(thread);

            threadController tc = new threadController();

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), tc.info("slug"));
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), tc.info("0"));
        }
    }

    @Test
    @DisplayName("Test voting")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User user = new User("user", "email", "name", "desc");
                Vote vote = new Vote("user", 1);

                userMock.when(() -> UserDAO.Info("user")).thenReturn(user);
                threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

                threadController tc = new threadController();

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(), tc.createVote("slug", vote).getStatusCode());
            }
        }
    }
}

